﻿using Domain.Models.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Domain.Data
{
    public class BlueContext : DbContext
    {
        public BlueContext(DbContextOptions<BlueContext> options) : base(options)
        { }

        public DbSet<Agenda> Agenda { get; set; }
        public DbSet<Login> Login { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agenda>().ToTable("Agenda");
            modelBuilder.Entity<Login>().ToTable("Login");
        }
    }
}