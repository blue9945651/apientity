﻿using Domain.Models;
using Domain.Models.DTOs;

namespace Domain.Interfaces.Services
{
    public interface IServices
    {
        Task<LoginReturn> CheckLogin(string Login, string Password);
        Task<IEnumerable<Agenda>> GetAllAgenda(int ID);
        Task<IEnumerable<Agenda>> GetAgenda(int ID);
        Task<int> CreateAgenda(Agenda objeto);
        Task<int> UpdateAgenda(Agenda objeto);
        Task<int> DeleteAgenda(int ID);
    }
}
