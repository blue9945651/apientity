﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class LoginIn
    {
        [Required(ErrorMessage = "Login ser informado")]
        public string? Login { get; set; }
        
        [Required(ErrorMessage = "Senha ser informada")]
        public string? Password { get; set; }
    }
}
