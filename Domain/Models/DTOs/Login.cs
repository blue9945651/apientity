﻿namespace Domain.Models.DTOs
{
    public class Login
    {
        public int ID { get; set; }
        public string? NOME { get; set; }
        public string? LOGIN { get; set; }
        public string? SENHA { get; set; }
        public string? EMAIL { get; set; }
    }
}
