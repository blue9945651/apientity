﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Domain.Utils
{
    public static class Token
    {
        public static string GerarTokenJwt(IConfiguration _configuration, int UsuarioId, string Nome, string Email)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, UsuarioId.ToString()),
                new Claim(ClaimTypes.Name, Nome),
                new Claim(ClaimTypes.Email, Email)
            };

            var issuer = _configuration["Jwt:Issuer"];
            var audience = _configuration["Jwt:Audience"];
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(claims: claims, issuer: issuer, audience: audience, expires: DateTime.UtcNow.AddMinutes(30), notBefore: DateTime.UtcNow, signingCredentials: credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);
            return stringToken;
        }
    }
}
