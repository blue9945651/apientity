﻿using Domain.Data;
using Domain.Interfaces.Repositories;
using Domain.Models.DTOs;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace Repository
{
    public class Repository : IRepository
    {
        private readonly IConfiguration _confinguration;
        private readonly BlueContext _blueContext;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);
        private IDbConnection? DbConnection { get; set; }

        public Repository(IConfiguration confinguration, BlueContext blueContext)
        {
            _confinguration = confinguration;
            _blueContext = blueContext;
        }

        public async Task<IEnumerable<Login>> CheckLogin(string Login, string Password)
        {
            try
            {
                return await _blueContext.Login.Where(x => (x.LOGIN == Login || x.EMAIL == Login) && x.SENHA == Criptografia.EncryptPwd(Password)).ToListAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<IEnumerable<Agenda>> GetAllAgenda(int ID)
        {
            try
            {
                return await _blueContext.Agenda.Where(x => x.LoginId == ID).OrderBy(o => o.Nome).ToListAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<IEnumerable<Agenda>> Get(int ID)
        {
            try
            {
                return await _blueContext.Agenda.Where(x => x.Id == ID).ToListAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<IEnumerable<Agenda>> GetFromEmail(Agenda objeto)
        {
            try
            {
                return await _blueContext.Agenda.Where(x => x.LoginId == objeto.LoginId && (x.Email == objeto.Email || x.Nome == objeto.Nome)).ToListAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<IEnumerable<Agenda>> GetFromEmailInsteadThis(Agenda objeto)
        {
            try
            {
                return await _blueContext.Agenda.Where(x => x.Id != objeto.Id && (x.Email == objeto.Email || x.Nome == objeto.Nome)).ToListAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<int> CreateAgenda(Agenda objeto)
        {
            try
            {
                _blueContext.Agenda.Add(objeto);
                return await _blueContext.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<int> UpdateAgenda(Agenda objeto)
        {
            try
            {
                var entity = await _blueContext.Agenda.FindAsync(objeto.Id);
                _blueContext.Entry(entity).CurrentValues.SetValues(objeto);
                return await _blueContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }

        public async Task<int> DeleteAgenda(int ID)
        {
            try
            {
                await _blueContext.Agenda.Where(x => x.Id == ID).ExecuteDeleteAsync();
                return await _blueContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                throw;
            }
        }
    }
}
