﻿using Domain.Interfaces.Services;
using Domain.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace APIDapper.Controllers
{
    /// <summary>
    /// Classe para dados da agenda
    /// </summary>
    [Route("blue/[controller]/[action]")]
    [ApiController]
    public class AgendaController(IConfiguration configuration, IServices services) : ControllerBase
    {
        private readonly IConfiguration _configuration = configuration;
        private readonly IServices _services = services;

        /// <summary>
        /// Método buscar todas as agendas
        /// </summary>
        /// <returns>Objeto com dados das agendas</returns>
        /// <response code="200">Retorna os dados de agenda</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetAll(int ID)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                var retorno = await _services.GetAllAgenda(ID);
                return Ok(retorno.ToList());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Método buscar a agenda
        /// </summary>
        /// <returns>Objeto com dados das agendas</returns>
        /// <response code="200">Retorna os dados de agenda</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [Authorize]
        [HttpGet]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Get(int ID)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                var retorno = await _services.GetAgenda(ID);
                return Ok(retorno.ToList()[0]);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Método para incluir uma nova agenda
        /// </summary>
        /// <returns>True/false de alteração de agenda</returns>
        /// <response code="200">Agenda criada com sucesso</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [Authorize]
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Create([FromBody] Agenda objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                return Ok(await _services.CreateAgenda(objeto));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Método para alterar uma agenda
        /// </summary>
        /// <returns>True/false de alteração de agenda</returns>
        /// <response code="200">Agenda alterada com sucesso</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [Authorize]
        [HttpPut]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Update([FromBody] Agenda objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                return Ok(await _services.UpdateAgenda(objeto));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Método para remover uma agenda
        /// </summary>
        /// <returns>True/false de remoção de agenda</returns>
        /// <response code="200">Agenda removida com sucesso</response>
        /// <response code="204">Sem conteudo</response>
        /// <response code="400">Problemas com o model enviado</response>
        /// <response code="500">Internal server error</response>
        [Authorize]
        [HttpDelete]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> Delete(int ID)
        {
            if (!ModelState.IsValid)
                return BadRequest(string.Join("\r\n", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)));

            try
            {
                return Ok(await _services.DeleteAgenda(ID));
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
